import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.css']
})
export class AppointmentDetailComponent implements OnInit {

  appointment:any;

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router,private _location: Location) { }

  ngOnInit() {
    this.rest.getAppointment(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.appointment = data;
    });
  }

  onClickMe() {
    this._location.back();
  }

}
