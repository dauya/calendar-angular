import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { AppointmentComponent } from './appointment/appointment.component';
import { AppointmentDetailComponent } from './appointment-detail/appointment-detail.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'appointments',
    component: AppointmentComponent,
    data: { title: 'Appoitments List' }
  },
  {
    path: 'appointment-details/:id',
    component: AppointmentDetailComponent,
    data: { title: 'Appoitment Details' }
  },
  { path: '',
    redirectTo: '/appointments',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppointmentComponent,
    AppointmentDetailComponent
  ],
  imports: [RouterModule.forRoot(routes),CommonModule,MatListModule,MatCardModule,MatToolbarModule,MatButtonModule,MatIconModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
